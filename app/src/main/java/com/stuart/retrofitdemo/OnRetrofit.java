package com.stuart.retrofitdemo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Định nghĩa các Endpoint quan trọng
 */
public interface OnRetrofit {

    /**
     * @param id - identification of the employee
     * @return a EmployeeEntity obj
     */
    @GET("employee/{id}")
    Call<EmployeeEntity> getEmployee(@Path("id") int id);


    /**
     * @return all employees
     */
    @GET("employees")
    Call<List<EmployeeEntity>> geAllEmployees(@Query("sort") String sort); // không rõ @Query dùng để làm gì - cần demo

    /**
     * @param entity đối tượng cần tạo bản ghi
     * @return entity là đối tượng đả tạo
     */
    @POST("create")
    Call<EmployeeEntity> createEmployee(@Body EmployeeEntity entity); // không rõ @Body dùng để làm gì - cần demo

    /**
     * @param entity đối tượng chứa thông tin cần update
     * @param id id của bản ghi cần update
     * @return true - update thành công
     */
    @PUT("update/{id}")
    Call<Boolean> updateEmployeeById(@Body EmployeeEntity entity, @Path("id") int id);

    /**
     * @param id identification of the employee
     * @return true is update successful
     */
    @DELETE("delete/{id}")
    Call<Boolean> deleteEmployeeById(@Path("id") int id);
}
