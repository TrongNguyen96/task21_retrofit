package com.stuart.retrofitdemo;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitDemo {
    public static final String BASE_URL = "http://dummy.restapiexample.com/api/v1/";

    private Retrofit retrofit;

    /**
     * tạo đối tượng retrofit với converter là gson
     */
    public RetrofitDemo() {
        retrofit= new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
